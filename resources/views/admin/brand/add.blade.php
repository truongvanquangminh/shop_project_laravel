@extends('admin.layout.app')

@section('content')
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-5 align-self-center">
                <h4 class="page-title">Create Brand</h4>
            </div>
            <div class="col-7 align-self-center">
                <div class="d-flex align-items-center justify-content-end">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="#">Home</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Create Brand</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            @if ($errors->any())
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                    <h4><i class="icon fa fa-check"></i>Thong bao!</h4>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <label for="name">Name Brand <span style="color: red;">(*)</span></label>
                        <form action="{{ route('ad.brand.add') }}" class="form-horizontal form-material" method="post">
                            @csrf
                            <div class="form-group">
                                <input class="form-control" type="text" name="name" id="name">
                            </div>
                            <input type="submit" class="btn btn-success" value="Create Brand">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
