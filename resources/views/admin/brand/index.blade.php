@extends('admin.layout.app')

@section('content')
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-5 align-self-center">
                <h4 class="page-title">Brand</h4>
            </div>
            <div class="col-7 align-self-center">
                <div class="d-flex align-items-center justify-content-end">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="#">Home</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Brand</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-12 col-xlg-9 col-md-7">
        @if (session('success'))
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                <h4><i class="icon fa fa-check"></i>Thông báo!</h4>
                {{ session('success') }}
            </div>
        @endif
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="table-responsive">
                        <table class="table">
                            <thead class="thead-light">
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Name</th>
                                    <th scope="col">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($data as $key => $value)
                                    <tr>
                                        <th scope="row">{{ $key + 1 }}</th>
                                        <td>{{ $value['name'] }}</td>
                                        <td>
                                            <div class="btn-group">
                                                {{-- <a href="{{ route('ad.country.show', ['id' => $value['id']]) }}"
                                                    data-toggle="tooltip" data-original-title="Edit"
                                                    class="btn btn-xs btn-default"><i class="mdi mdi-tooltip-edit"></i></a> --}}
                                                <a href="{{ route('ad.brand.delete', ['id' => $value['id']]) }}"
                                                    data-toggle="tooltip" data-original-title="Delete"
                                                    class="btn btn-xs btn-danger"><i class="mdi mdi-delete"></i></a>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                {{ $data->links('vendor.pagination.bootstrap-4') }}
                <a href="{{ route('ad.brand.create') }}" class="btn btn-success">Add Brand</a>
            </div>
        </div>
    </div>
@endsection
