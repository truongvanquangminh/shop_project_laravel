@extends('admin.layout.app')

@section('content')
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-5 align-self-center">
                <h4 class="page-title">Update Blog</h4>
            </div>
            <div class="col-7 align-self-center">
                <div class="d-flex align-items-center justify-content-end">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="#">Home</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Update Blog</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            @if ($errors->any())
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                    <h4><i class="icon fa fa-check"></i>Thông báo!</h4>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <div class="col-12">
                <div class="card">
                    @foreach ($data as $item)
                        <form action="{{ route('ad.blog.edit', $id) }}" class="form-horizontal form-material" method="post"
                            enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <label for="title" class="col-md-12">Tilte <span style="color: red;">(*)</span></label>
                                <div class="col-md-12">
                                    <input class="form-control" type="text" name="tilte" id="title"
                                        value="{{ $item->title }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12">Image</label>
                                <div class="col-md-12">
                                    <input type="file" name="image" value="{{ $item->image }}" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="desc" class="col-md-12">Description</label>
                                <div class="col-md-12">
                                    <textarea class="form-control" name="desc" id="desc">{{ $item->description }}</textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="content" class="col-md-12">Content</label>
                                <div class="col-md-12">
                                    <textarea class="form-control ckeditor" name="content" id="demo">{{ $item->content }}</textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-12">
                                    <input type="submit" class="btn btn-success" value="Update Blog">
                                </div>
                            </div>
                        </form>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection
