@extends('frontend.layouts.app')
@section('title', 'Checkout')
@section('content')
    <section id="cart_items">
        <div class="container">
            <div class="breadcrumbs">
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li class="active">Check out</li>
                </ol>
            </div>
            <!--/breadcrums-->
            @if (session('success'))
                <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                    <h4><i class="icon fa fa-check"></i>Thông báo!</h4>
                    {{ session('success') }}
                </div>
            @endif
            @if (!auth()->check())
                <div class="row">
                    <div class="col-sm-4"></div>
                    <div class="col-sm-4">
                        <div class="signup-form">
                            <!--sign up form-->
                            <h2>Please Register!</h2>
                            <form action="{{ route('shop.sendmail1') }}" method="post" enctype="multipart/form-data">
                                @csrf
                                <input type="text" name="name" placeholder="Name" />
                                <input type="email" name="email" placeholder="Email Address" />
                                <input type="password" name="password" placeholder="Password" />
                                <input type="text" name="phone" placeholder="Phone" />
                                <input type="text" name="address" placeholder="Address" />
                                <input type="file" name="avatar">
                                <button type="submit" class="btn btn-default">Signup</button>
                            </form>
                        </div>
                        <!--/sign up form-->
                    </div>
                    <div class="col-sm-4"></div>
                </div>
            @endif

            <div class="review-payment">
                <h2>Review & Payment</h2>
            </div>

            <div class="table-responsive cart_info">
                <table class="table table-condensed">
                    <thead>
                        <tr class="cart_menu">
                            <td class="image">Item</td>
                            <td class="description">Name</td>
                            <td class="price">Price</td>
                            <td class="quantity">Quantity</td>
                            <td class="total">Total</td>
                            <td></td>
                        </tr>
                    </thead>
                    <tbody>
                        @if (session()->has('cart'))
                            @php
                                $carts = session()->get('cart');
                                $total = 0;
                            @endphp
                            @foreach ($carts as $key => $value)
                                @php
                                    $sum = $value['price'] * $value['qty'];
                                    $total += $sum;
                                @endphp
                                <tr>
                                    <td class="cart_product">
                                        <a href=""><img
                                                src="../../upload/product/{{ $value['user_id'] }}/{{ date_format($value['created_at'], 'Y/m/d') . '/hinh85_' . $value['img'] }}"
                                                alt=""></a>
                                    </td>
                                    <td class="cart_description">
                                        <h4><a href="">{{ $value['name'] }}</a></h4>
                                        <p>Web ID: {{ $key }}</p>
                                    </td>
                                    <td class="cart_price">
                                        <p>${{ $value['price'] }}</p>
                                    </td>
                                    <td class="cart_quantity">
                                        <div class="cart_quantity_button">
                                            <a class="cart_quantity_up" href=""> + </a>
                                            <input class="cart_quantity_input" type="text" name="quantity"
                                                value="{{ $value['qty'] }}" autocomplete="off" size="2">
                                            <a class="cart_quantity_down" href=""> - </a>
                                        </div>
                                    </td>
                                    <td class="cart_total">
                                        <p class="cart_total_price">${{ $sum }}</p>
                                    </td>
                                    <td class="cart_delete">
                                        <a class="cart_quantity_delete" href=""><i class="fa fa-times"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                        @endif

                        <tr>
                            <td colspan="4">&nbsp;</td>
                            <td colspan="2">
                                <table class="table table-condensed total-result">
                                    <tr>
                                        <td>Shipping Cost</td>
                                        <td>Free</td>
                                    </tr>
                                    <tr>
                                        <td>Total</td>
                                        <td><span>${{ $total }}</span></td>
                                    </tr>
                                    <tr class="btn-order">
                                        <td>
                                            <a class="btn btn-primary btn_order"
                                                href=" {{ route('shop.sendmail') }}">Place order now</a>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="payment-options">
                <span>
                    <label><input type="checkbox"> Direct Bank Transfer</label>
                </span>
                <span>
                    <label><input type="checkbox"> Check Payment</label>
                </span>
                <span>
                    <label><input type="checkbox"> Paypal</label>
                </span>
            </div>
        </div>
    </section>
    <!--/#cart_items-->
@endsection
