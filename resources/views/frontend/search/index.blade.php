@extends('frontend.layouts.master')
@section('title', 'Search Advanced')
@section('content')
    <style>
        select {
            width: 166px;
            height: 33px;
        }

        .nameProduct {
            margin-left: 15px;
            width: 155px;
            border: none;
            background: #F0F0E9;
            height: 33px;
            padding-left: 10px;
        }

        .btn.btn-search {
            margin: 15px;
            background: #FE980F;
            color: #fff;
        }

    </style>
    <div class="col-sm-9 padding-right">
        <div class="features_items">
            <!--features_items-->
            <h2 class="title text-center">Product</h2>
            <div class="form_search">
                <form action="{{ route('shop.searchAvanced') }}" method="get">
                    @csrf
                    <input class="nameProduct" type="text" name="nameProduct" placeholder="Name Product">
                    <select name="price">
                        <option value="" selected>Choose price</option>
                        <option value="50-1000">$50-$1000</option>
                        <option value="1000-5000">$1000-$5000</option>
                        <option value="5000-10000">$5000-$10000</option>
                        <option value="10000-20000">$10000-$20000</option>
                    </select>
                    <select name="category" id="">
                        <option value="" selected>Category</option>
                        @foreach ($categories as $category)
                            <option value="{{ $category['id'] }}">{{ $category['name'] }}</option>
                        @endforeach
                    </select>
                    <select name="brand" id="">
                        <option value="" selected>Brand</option>
                        @foreach ($brands as $brand)
                            <option value="{{ $brand['id'] }}">{{ $brand['name'] }}</option>
                        @endforeach
                    </select>
                    <select name="status" id="">
                        <option value="" selected>Status</option>
                        <option value="0">New</option>
                        <option value="1">Sale</option>
                    </select>
                    <button id="btn_search" class="btn btn-search" type="submit">Search</button>
                </form>
            </div>
            <div id="view">
                @foreach ($products as $item)
                    <div class="col-sm-4">
                        <div class="product-image-wrapper">
                            <div class="single-products">
                                <div class="productinfo text-center">
                                    <a href="{{ route('shop.product-detail', $item->id) }}"><img
                                            src="{{ asset('upload/product/' . $item->user_id . '/' . date_format($item->created_at, 'Y/m/d') . '/' . current(json_decode($item['images']))) }}"
                                            alt="" /></a>
                                    <h2>${{ $item->price }}</h2>
                                    <p title="{{ $item->name }}"
                                        style="text-overflow: ellipsis;overflow: hidden;margin: 0 10px;line-height: 21px;display: -webkit-box;-webkit-line-clamp: 1;-webkit-box-orient: vertical;">
                                        {{ $item->name }}</p>
                                    <a href="" id="{{ $item->id }}" class="btn btn-default add-to-cart"><i
                                            class="fa fa-shopping-cart"></i>Add to
                                        cart</a>
                                </div>
                            </div>
                            <div class="choose">
                                <ul class="nav nav-pills nav-justified">
                                    <li><a href="#"><i class="fa fa-plus-square"></i>Add to wishlist</a></li>
                                    <li><a href="#"><i class="fa fa-plus-square"></i>Add to compare</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        {{-- {{ $products->links('vendor.pagination.bootstrap-4') }} --}}

    </div>
@endsection
