@extends('frontend.layouts.master')
@section('title', 'Product Details')
@section('content')
    <div class="col-sm-9 padding-right">
        <div class="product-details">
            <!--product-details-->
            <div class="col-sm-5">
                <div class="view-product">
                    <img class="imgFull"
                        src="../../upload/product/{{ $product->user_id }}/{{ date_format($product->created_at, 'Y/m/d') . '/' . current(json_decode($product['images'])) }}" />
                    <a class="hrefFull"
                        href="../../upload/product/{{ $product->user_id }}/{{ date_format($product->created_at, 'Y/m/d') . '/' . current(json_decode($product['images'])) }}"
                        rel="prettyPhoto">
                        <h3>ZOOM</h3>
                    </a>
                </div>
                <div id="similar-product" class="carousel slide" data-ride="carousel">

                    <?php $getArrImage = json_decode($product['images'], true); ?>
                    <!-- Wrapper for slides -->
                    <div class="carousel-inner">
                        <div class="item active">
                            @foreach ($getArrImage as $item)
                                {{-- {{ dd('upload/product/' . $product->user_id . '/' . date_format($product->created_at, 'Y/m/d') . '/' . 'hinh85_' . $item) }} --}}
                                <a href=""><img class="imgActive"
                                        src="../../upload/product/{{ $product->user_id }}/{{ date_format($product->created_at, 'Y/m/d') . '/hinh85_' . $item }}"
                                        alt=""></a>
                            @endforeach
                        </div>
                        <div class="item">
                            @foreach ($getArrImage as $item)
                                <a href=""><img class="imgActive"
                                        src="../../upload/product/{{ $product->user_id }}/{{ date_format($product->created_at, 'Y/m/d') . '/hinh85_' . $item }}"
                                        alt=""></a>
                            @endforeach
                        </div>
                    </div>

                    <!-- Controls -->
                    <a class="left item-control" href="#similar-product" data-slide="prev">
                        <i class="fa fa-angle-left"></i>
                    </a>
                    <a class="right item-control" href="#similar-product" data-slide="next">
                        <i class="fa fa-angle-right"></i>
                    </a>
                </div>

            </div>
            <div class="col-sm-7">
                <div class="product-information">
                    <!--/product-information-->
                    <img src="images/product-details/new.jpg" class="newarrival" alt="" />
                    <h2>{{ $product['name'] }}</h2>
                    {{-- <p>Web ID: 1089772</p> --}}
                    <img src="images/product-details/rating.png" alt="" />
                    <span>
                        <span>${{ $product['price'] }}</span>
                        {{-- <label>Quantity:</label>
                        <input type="text" value="3" /> --}}
                        <button type="button" id="{{$product['id']}}" class="btn btn-fefault cart add-to-cart">
                            <i class="fa fa-shopping-cart"></i>
                            Add to cart
                        </button>
                    </span>
                    {{-- <p><b>Availability:</b> In Stock</p> --}}
                    <p><b>Category:</b>
                        @foreach ($category as $key => $item)
                            @if ($key == $product->category_id)
                                {{ $item->name }}
                            @endif
                        @endforeach
                    </p>
                    <p><b>Brand:</b>
                        @foreach ($brand as $key => $item)
                            @if ($key == $product->brand_id)
                                {{ $item->name }}
                            @endif
                        @endforeach
                    </p>
                    <a href=""><img src="images/product-details/share.png" class="share img-responsive" alt="" /></a>
                </div>
                <!--/product-information-->
            </div>
        </div>
        <!--/product-details-->
    </div>
    <script>
        $(document).ready(function() {
            $('.imgActive').click(function() {
                var valImgActive = $(this).attr('src');
                valImgActive = valImgActive.replace(
                    "../../upload/product/{{ $product->user_id }}/{{ date_format($product->created_at, 'Y/m/d') }}/hinh85_",
                    '');
                $('.imgFull').attr(
                    'src',
                    '../../upload/product/{{ $product->user_id }}/{{ date_format($product->created_at, 'Y/m/d') }}/' +
                    valImgActive)
                $('.hrefFull').attr(
                    'href',
                    '../../upload/product/{{ $product->user_id }}/{{ date_format($product->created_at, 'Y/m/d') }}/' +
                    valImgActive
                )
                return false;
            })
        })
    </script>
@endsection
