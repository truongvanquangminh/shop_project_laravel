@extends('frontend.layouts.master')
@section('title', 'Home')
@section('content')
    <div class="col-sm-9 padding-right">
        <div class="features_items">
            <!--features_items-->
            <h2 class="title text-center">Product</h2>

            <div id="view">
                @foreach ($getAllProduct as $item)
                    <div class="col-sm-4">
                        <div class="product-image-wrapper">
                            <div class="single-products">
                                <div class="productinfo text-center">
                                    <a href="{{ route('shop.product-detail', $item['id']) }}">
                                        {{-- <img
                                            src="{{ asset('upload/product/' . $item['user_id'] . '/' . date_format($item['created_at'], 'Y/m/d') . '/' . current(json_decode($item['images']))) }}"
                                            alt="" /> --}}
                                        </a>
                                    <h2>${{ $item['price'] }}</h2>
                                    <p title="{{ $item['name'] }}"
                                        style="text-overflow: ellipsis;overflow: hidden;margin: 0 10px;line-height: 21px;display: -webkit-box;-webkit-line-clamp: 1;-webkit-box-orient: vertical;">
                                        {{ $item['name'] }}</p>
                                    <a href="" id="{{ $item['id'] }}" class="btn btn-default add-to-cart"><i
                                            class="fa fa-shopping-cart"></i>Add to
                                        cart</a>
                                </div>
                            </div>
                            <div class="choose">
                                <ul class="nav nav-pills nav-justified">
                                    <li><a href="#"><i class="fa fa-plus-square"></i>Add to wishlist</a></li>
                                    <li><a href="#"><i class="fa fa-plus-square"></i>Add to compare</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>

    </div>
@endsection
