@extends('frontend.layouts.app')
@section('title', 'Add Product')
@section('content')
    <div class="col-sm-3">
        <div class="left-sidebar">
            <h2>Account</h2>
            <div class="panel-group category-products">
                <!--category-productsr-->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a href="{{ route('shop.account') }}">
                                <span class="badge pull-right"><i class="fa fa-plus"></i></span>
                                Account
                            </a>
                        </h4>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a href="{{ route('shop.product') }}">
                                <span class="badge pull-right"><i class="fa fa-plus"></i></span>
                                My Product
                            </a>
                        </h4>
                    </div>
                </div>
            </div>
            <!--/category-products-->
        </div>
    </div>
    <div class="col-sm-9">
        <div class="blog-post-area">
            <div class="row">
                <div class="col-sm-2"></div>
                <div class="col-sm-8">
                    <div class="signup-form">
                        <p>Create product!</p>
                        @if (session('success'))
                            <div class="alert alert-success alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert"
                                    aria-hidden="true">x</button>
                                <h4><i class="icon fa fa-check"></i>Thông báo!</h4>
                                {{ session('success') }}
                            </div>
                        @endif

                        @if ($errors->any())
                            <div class="alert alert-danger alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert"
                                    aria-hidden="true">x</button>
                                <h4><i class="icon fa fa-check"></i>Thông báo!</h4>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <form action="{{ route('shop.product.add') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="">
                            <input type="text" placeholder="Name" name="name">
                            <input type="text" placeholder="Price" name="price">
                            <select name="category">
                                <option value="" selected>Please choose category</option>
                                @foreach ($category as $item)
                                    <option value="{{ $item['id'] }}">{{ $item['name'] }}</option>
                                @endforeach
                            </select>
                            <select name="brand">
                                <option value="" selected>Please choose brand</option>
                                @foreach ($brand as $item)
                                    <option value="{{ $item['id'] }}">{{ $item['name'] }}</option>
                                @endforeach
                            </select>
                            <select name="status" id="status">
                                <option value="0" selected>New</option>
                                <option value="1">Sale</option>
                            </select>
                            <input type="text" class="hidden" name='sale' id="sale" placeholder="0%" style="width:40%">
                            <input type="text" name="company" placeholder="Company profile">
                            <input type="file" name="image[]" multiple>
                            <textarea name="detail" placeholder="Detail" cols="30" rows="10"></textarea>
                            <input class="btn btn-primary" type="submit" value="Add">
                        </form>
                    </div>
                </div>
                <div class="col-sm-2"></div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function() {
            $('#status').click(function() {
                $status = $(this).children("option:selected").val();
                if ($status == 1) {
                    $('input#sale').attr('class', '');
                } else {
                    $('input#sale').attr('class', 'hidden');
                }
            })
        })
    </script>
@endsection
