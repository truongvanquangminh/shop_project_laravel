@extends('frontend.layouts.app')
@section('title', 'My Product')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                <div class="left-sidebar">
                    <h2>Account</h2>
                    <div class="panel-group category-products">
                        <!--category-productsr-->
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a href="{{ route('shop.account') }}">
                                        <span class="badge pull-right"><i class="fa fa-plus"></i></span>
                                        Account
                                    </a>
                                </h4>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a href="{{ route('shop.product') }}">
                                        <span class="badge pull-right"><i class="fa fa-plus"></i></span>
                                        My Product
                                    </a>
                                </h4>
                            </div>
                        </div>
                    </div>
                    <!--/category-products-->
                </div>
            </div>
            <div class="col-sm-9">
                <div class="blog-post-area">
                    <section id="cart_items">
                        <div class="table-responsive cart_info" style="margin-bottom: 10px;">
                            <table class="table table-condensed">
                                <thead>
                                    <tr class="cart_menu">
                                        <td class="id">ID</td>
                                        <td class="name">Name</td>
                                        <td class="image">Image</td>
                                        <td class="price">Price</td>
                                        <td class="action">Action</td>
                                        <td></td>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse ($products as $key => $item)
                                        <?php
                                        // $getArrImage = json_decode($item['images'], true);
                                        ?>
                                        <tr>
                                            <td>
                                                <a>{{ $key + 1 }}</a>
                                            </td>
                                            <td>
                                                <h4><a>{{ $item->name }}</a></h4>
                                            </td>
                                            <td class="cart_product">
                                                <a><img src="{{ asset('upload/product/' . $item->user_id . '/' . ($date = date('Y/m/d') . '/' . current(json_decode($item['images'])))) }}"
                                                        alt="" style="width: 100px;height: 100px;padding: 5px;"></a>
                                            </td>
                                            <td class="cart_price">
                                                <p>${{ $item->price }}</p>
                                            </td>
                                            <td>
                                                <a href="{{ route('shop.product.show', $item->id) }}"><i
                                                        class="fa fa-pencil"></i></a>
                                            </td>
                                            <td>
                                                <a href="{{ route('shop.product.delete', $item->id) }}"><i
                                                        class="fa fa-times"></i></a>
                                            </td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td colspan="5" style="text-align: center;">Không có sản phẩm</td>
                                        </tr>
                                    @endforelse

                                </tbody>
                            </table>
                        </div>
                    </section>
                </div>
                <div class="form-group col-md-12">
                    <a href="{{ route('shop.product.create') }}" class="btn btn-primary pull-right">Add</a>
                </div>
            </div>
        </div>
    </div>
@endsection
