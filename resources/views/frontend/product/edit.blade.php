@extends('frontend.layouts.app')
@section('title', 'Update Product')
@section('content')
    <div class="col-sm-3">
        <div class="left-sidebar">
            <h2>Account</h2>
            <div class="panel-group category-products">
                <!--category-productsr-->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a href="{{ route('shop.account') }}">
                                <span class="badge pull-right"><i class="fa fa-plus"></i></span>
                                Account
                            </a>
                        </h4>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a href="{{ route('shop.product') }}">
                                <span class="badge pull-right"><i class="fa fa-plus"></i></span>
                                My Product
                            </a>
                        </h4>
                    </div>
                </div>
            </div>
            <!--/category-products-->
        </div>
    </div>
    <div class="col-sm-9">
        <div class="blog-post-area">
            <div class="row">
                <div class="col-sm-2"></div>
                <div class="col-sm-8">
                    <div class="signup-form">
                        <p>Update product!</p>
                        @if (session('success'))
                            <div class="alert alert-success alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert"
                                    aria-hidden="true">x</button>
                                <h4><i class="icon fa fa-check"></i>Thông báo!</h4>
                                {{ session('success') }}
                            </div>
                        @endif

                        @if ($errors->any())
                            <div class="alert alert-danger alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert"
                                    aria-hidden="true">x</button>
                                <h4><i class="icon fa fa-check"></i>Thông báo!</h4>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <form action="{{ route('shop.product.edit', $id) }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <input type="text" placeholder="Name" value="{{ $product->name }}" name="name">
                            <input type="text" placeholder="Price" value="{{ $product->price }}" name="price">
                            <select name="category">
                                @foreach ($categories as $category)
                                    <option value="{{ $category['id'] }}"
                                        {{ $category['id'] == $product['category_id'] ? 'selected' : '' }}>
                                        {{ $category['name'] }}</option>
                                @endforeach
                            </select>
                            <select name="brand">
                                @foreach ($brands as $brand)
                                    <option value="{{ $brand['id'] }}"
                                        {{ $brand['id'] == $product['brand_id'] ? 'selected' : '' }}>
                                        {{ $brand['name'] }}</option>
                                @endforeach
                            </select>
                            <select name="status" id="status">
                                @if ($product->status == 0)
                                    <option value="0" selected> New</option>
                                    <option value="1">Sale</option>
                                @else
                                    <option value="0"> New</option>
                                    <option value="1" selected>Sale</option>
                                @endif
                            </select>
                            <input type="text" name="sale" id="sale" value="{{ $product->sale }}" style="width:40%">
                            <input type="text" name="company" value="{{ $product->company }}">
                            <input type="file" name="image[]" multiple>
                            <ul style="display: flex;">
                                <?php $getArrImage = json_decode($product['images'], true); ?>
                                @foreach ($getArrImage as $key => $value)
                                    <li style="padding-right: 10px;"><img style="width: 50px;height: 50px;padding: 5px;"
                                            src="{{ asset('upload/product/' . Auth::user()->id . '/' . ($date = date('Y/m/d') . '/' . $value)) }} "
                                            alt="" /></li>
                                    <input type="checkbox" name="images[]" value="{{ $value }}"
                                        style="width: 20px;">
                                @endforeach
                            </ul>
                            <textarea name="detail" cols="30" rows="10">{{ $product->detail }}</textarea>
                            <input class="btn btn-primary" type="submit" value="Update">
                        </form>
                    </div>
                </div>
                <div class="col-sm-2"></div>
            </div>
        </div>
    </div>
    <script>
        // $(document).ready(function() {
        //     $('#status').change(function() {
        //         $status = $(this).children("option:selected").val();
        //         if ($status == 1) {
        //             $('input#sale').attr('class', '');
        //         } else {
        //             $('input#sale').attr('class', 'hidden');
        //         }
        //     })
        // })
    </script>
@endsection
