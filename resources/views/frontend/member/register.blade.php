@extends('frontend.layouts.app')
@section('title', 'Register')
@section('content')
    <div class="col-sm-3"></div>
    <div class="col-sm-6" style="margin-bottom: 20px;">
        <div class="signup-form">
            <!--sign up form-->
            <h2>New User Signup!</h2>
            @if (session('success'))
                <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                    <h4><i class="icon fa fa-check"></i>Thong bao!</h4>
                    {{ session('success') }}
                </div>
            @endif
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form action="" method="post" enctype="multipart/form-data">
                @csrf
                <input type="text" name="name" placeholder="Name" />
                <input type="email" name="email" placeholder="Email Address" />
                <input type="password" name="password" placeholder="Password" />
                <input type="text" name="phone" placeholder="Phone" />
                <input type="text" name="address" placeholder="Address" />
                <input type="file" name="avatar">
                <button type="submit" class="btn btn-default">Signup</button>
            </form>
        </div>
        <!--/sign up form-->
    </div>
    <div class="col-sm-3"></div>
@endsection
