@extends('frontend.layouts.app')
@section('title', 'Login')
@section('content')
    <div class="col-sm-4"></div>
    <div class="col-sm-4" style="margin-bottom: 20px;">
        <div class="login-form">
            <!--login form-->
            <h2>Login to your account</h2>

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form action="{{ route('shop.login') }}" method="post">
                @csrf
                <input type="email" name="email" placeholder="Email Address" />
                <input type="password" name="password" placeholder="Enter Password" />
                <div>
                    <span>
                        <input type="checkbox" name="remeber_me" class="checkbox">
                        Keep me signed in.
                        <a href="{{ route('shop.register') }}" style="margin-left:20px">Register</a>
                    </span>
                </div>
                <button type="submit" class="btn btn-default">Login</button>
            </form>
        </div>
        <!--/login form-->
    </div>
    <div class="col-sm-4"></div>
@endsection
