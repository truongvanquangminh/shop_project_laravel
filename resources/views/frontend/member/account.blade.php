@extends('frontend.layouts.app')

@section('title', 'Account')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                <div class="left-sidebar">
                    <h2>Account</h2>
                    <div class="panel-group category-products">
                        <!--category-productsr-->
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a href="{{ route('shop.account') }}">
                                        <span class="badge pull-right"><i class="fa fa-plus"></i></span>
                                        Account
                                    </a>
                                </h4>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a href="{{ route('shop.product') }}">
                                        <span class="badge pull-right"><i class="fa fa-plus"></i></span>
                                        My Product
                                    </a>
                                </h4>
                            </div>
                        </div>
                    </div>
                    <!--/category-products-->
                </div>
            </div>
            <div class="col-sm-3">
                <div class="card">
                    <div class="card-body">
                        <center class="m-t-30">
                            <img src="{{ asset('upload/user/avatar/' . Auth::user()->avatar) }}"
                                style="border-radius: 50%!important;" width="150" />
                        </center>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="blog-post-area">
                    <div class="row">
                        <div class="shopper-info">
                            <p>User update!</p>
                            @if (session('success'))
                                <div class="alert alert-success alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert"
                                        aria-hidden="true">x</button>
                                    <h4><i class="icon fa fa-check"></i>Thông báo!</h4>
                                    {{ session('success') }}
                                </div>
                            @endif
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <form action="" method="POST" enctype="multipart/form-data">
                                @csrf
                                Name: <input type="text" name="name" value="{{ Auth::user()->name }}">
                                Email: <input type="text" name="email" value="{{ Auth::user()->email }}">
                                Password: <input type="password" name="password" value="{{ Auth::user()->password }}">
                                Address: <input type="text" name="address" value="{{ Auth::user()->address }}">
                                Country:
                                <select name="country">
                                    @foreach ($data as $key => $item)
                                        <option value="{{ $item['id'] }}" <?php
echo $item['id'] == Auth::User()->country ? 'selected' : '';
?>>
                                            {{ $item['name'] }}
                                        </option>
                                    @endforeach
                                </select>
                                Phone: <input type="text" name="phone" value="{{ Auth::user()->phone }}">
                                Avatar: <input style="padding-top: 10px;" type="file" name="avatar" id="avatar">
                                <input class="btn btn-primary" type="submit" name="update" value="Update">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
