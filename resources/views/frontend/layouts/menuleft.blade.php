<div class="col-sm-3">
    <div class="left-sidebar">
        <h2>Category</h2>
        <div class="panel-group category-products" id="accordian">
            <!--category-productsr-->
            @foreach ($category as $key => $value)
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title"><a
                                href="{{ url('/product/category/' . $value['id']) }}">{{ $value['name'] }}</a></h4>
                    </div>
                </div>
            @endforeach
        </div>
        <!--/category-products-->

        <div class="brands_products">
            <!--brands_products-->
            <h2>Brands</h2>
            <div class="brands-name">
                <ul class="nav nav-pills nav-stacked">
                    @foreach ($brand as $key => $value)
                        <li><a href="{{ url('/product/brand/' . $value['id']) }}">{{ $value['name'] }}</a></li>
                    @endforeach
                </ul>
            </div>
        </div>
        <!--/brands_products-->

        <div class="price-range">
            <!--price-range-->
            <h2>Price Range</h2>
            <div class="well text-center">
                <input type="text" class="span2" value="" data-slider-min="0" data-slider-max="200000"
                    data-slider-step="5" data-slider-value="[5000,100000]" id="sl2"><br />
                <b class="pull-left">$ 0</b> <b class="pull-right">$ 200000</b>
            </div>
        </div>
        <!--/price-range-->

    </div>
</div>

<script>
    $(document).ready(function() {
        $('.price-range').click(function() {
            var priceRange = $(this).find('.tooltip-inner').text();
            priceRange = priceRange.split(":");
            var minPrice = priceRange[0];
            var maxPrice = priceRange[1];

            $.ajax({
                type: "GET",
                url: "{{ route('shop.priceRange') }}",
                data: {
                    _token: "{{ csrf_token() }}",
                    minPrice: minPrice,
                    maxPrice: maxPrice
                },
                success: function(response) {
                    //     Object.keys(data).map((key, index) => {
                    //         Object.keys(data[key]).map((key1, index) => {
                    //             $('.productinfo p').html(data[key][key1].name)
                    //         })
                    //     })
                    console.log()
                    $('#view').html(response);
                }
            })
        })
    })
</script>
