<!DOCTYPE html>
<html lang="en">
{{-- Page login, register --}}

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>@yield('title') | E-Shopper</title>
    <link href="{{ asset('frontend/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/css/prettyPhoto.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/css/price-range.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/css/animate.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/css/main.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/css/responsive.css') }}" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
    <link rel="shortcut icon" href="{{ asset('frontend/images/ico/favicon.ico') }}">
    <link rel="apple-touch-icon-precomposed" sizes="144x144"
        href="{{ asset('frontend/images/ico/apple-touch-icon-144-precomposed.png') }}">
    <link rel="apple-touch-icon-precomposed" sizes="114x114"
        href="{{ asset('frontend/images/ico/apple-touch-icon-114-precomposed.png') }}">
    <link rel="apple-touch-icon-precomposed" sizes="72x72"
        href="{{ asset('frontend/images/ico/apple-touch-icon-72-precomposed.png') }}">
    <link rel="apple-touch-icon-precomposed"
        href="{{ asset('frontend/images/ico/apple-touch-icon-57-precomposed.png') }}">
    <script src="{{ asset('frontend/js/jquery.js') }}"></script>
    <script src="{{ asset('frontend/js/bootstrap.min.js') }}"></script>
</head>
<!--/head-->

<body>
    @include('frontend.layouts.header')

    <div class="container">
        <div class="row">
            @yield('content')
        </div>
    </div>

    @include('frontend.layouts.footer')

    <script>
        $(document).ready(function() {
            var tong = 0;
            $('.cart_quantity_up').click(function() {
                var id = $(this).closest('tr').attr('id');
                var price = $(this).closest('tr').find('td.cart_price').text();
                price = price.replace("$", "");
                var qty = $(this).closest('div.cart_quantity_button').find('input').attr('value');
                qty = parseInt(qty) + 1;
                $(this).closest('div.cart_quantity_button').find('input').attr('value', qty);
                var sumPrice = price * qty;
                $(this).closest('tr').find('p.cart_total_price').html('$' + sumPrice);
                $.ajax({
                    type: "POST",
                    url: "{{ route('shop.cartQtyUp') }}",
                    data: {
                        _token: "{{ csrf_token() }}",
                        idProduct: id
                    },
                    success: function(data) {
                        // $('span.total').text("$" + data.total);
                        $('.total_area #subTotal span').text(`$${data.total}`);
                        $('.total_area #total span').text(`$${data.total}`);
                    }
                })
                return false;
            })

            $('.cart_quantity_down').click(function() {
                var id = $(this).closest('tr').attr('id');
                var price = $(this).closest('tr').find('td.cart_price').text();
                price = price.replace("$", "");
                var qty = $(this).closest('div.cart_quantity_button').find('input').attr('value');
                qty = parseInt(qty) - 1;
                $(this).closest('div.cart_quantity_button').find('input').attr('value', qty);
                var sumPrice = price * qty;
                $(this).closest('tr').find('p.cart_total_price').html('$' + sumPrice);
                if (qty < 1) {
                    $(this).closest('tr').remove();
                }
                $.ajax({
                    type: "POST",
                    url: "{{ route('shop.cartQtyDown') }}",
                    data: {
                        _token: "{{ csrf_token() }}",
                        idProduct: id
                    },
                    success: function(data) {
                        // $('span.total').text("$" + data.total);
                        $('.total_area #subTotal span').text(`$${data.sum}`);
                        $('.total_area #total span').text(`$${data.sum}`);
                    }
                })
                return false;
            })

            $('.cart_quantity_delete').click(function() {
                $(this).closest('tr').remove();
                var id = $(this).closest('tr').attr('id');
                var total = $(this).closest("tr").find(".cart_total_price").text();
                var getTotal = total.split("$");
                $.ajax({
                    type: "GET",
                    url: "{{ route('shop.deleteCart') }}",
                    data: {
                        _token: "{{ csrf_token() }}",
                        idProduct: id
                    },
                    success: function(data) {
                        console.log(data.total);
                        $('.total_area #subTotal span').text(`$${data.total}`);
                        $('.total_area #total span').text(`$${data.total}`);
                    }
                })
                return false;
            })
        })
    </script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="{{ asset('frontend/js/jquery.scrollUp.min.js') }}"></script>
    <script src="{{ asset('frontend/js/price-range.js') }}"></script>
    <script src="{{ asset('frontend/js/jquery.prettyPhoto.js') }}"></script>
    <script src="{{ asset('frontend/js/main.js') }}"></script>
</body>

</html>
