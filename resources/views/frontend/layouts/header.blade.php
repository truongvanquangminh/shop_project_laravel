<header id="header">
    <!--header-->
    <div class="header_top">
        <!--header_top-->
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="contactinfo">
                        <ul class="nav nav-pills">
                            <li><a href="#"><i class="fa fa-phone"></i> +84 328 687 882</a></li>
                            <li><a href="#"><i class="fa fa-envelope"></i> minhtruong20002@gmail.com</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="social-icons pull-right">
                        <ul class="nav navbar-nav">
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                            <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                            <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--/header_top-->

    <div class="header-middle">
        <!--header-middle-->
        <div class="container">
            <div class="row">
                <div class="col-md-4 clearfix">
                    <div class="logo pull-left">
                        <a href="{{ route('index') }}"><img src="{{ asset('frontend/images/home/logo.png') }}"
                                alt="" /></a>
                    </div>
                    <div class="btn-group pull-right clearfix">
                        <div class="btn-group">
                            <button type="button" class="btn btn-default dropdown-toggle usa" data-toggle="dropdown">
                                USA
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu">
                                <li><a href="">Canada</a></li>
                                <li><a href="">UK</a></li>
                            </ul>
                        </div>

                        <div class="btn-group">
                            <button type="button" class="btn btn-default dropdown-toggle usa" data-toggle="dropdown">
                                DOLLAR
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu">
                                <li><a href="">Canadian Dollar</a></li>
                                <li><a href="">Pound</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-8 clearfix">
                    <div class="shop-menu clearfix pull-right">
                        <ul class="nav navbar-nav">
                            <li>
                                @if (!Auth::check())
                                    <a href="{{ route('shop.login') }}"><i class="fa fa-user"></i> Account</a>
                                @else
                                    <a href="{{ route('shop.account') }}"><i
                                            class="fa fa-user"></i>{{ Auth::User()->name }}</a>
                                @endif
                            </li>
                            @if (session()->get('cart'))
                                <li><a href="{{ route('shop.checkout') }}"><i class="fa fa-crosshairs"></i>
                                        Checkout</a>
                            @endif
                            </li>
                            <li><a href="{{ route('shop.showCart') }}"><i class="fa fa-shopping-cart"></i>
                                    <span class="countCart">
                                        @if (session()->has('cart'))
                                            {{ count(session()->get('cart')) }}
                                        @endif
                                    </span>
                                    Cart</a></li>
                            <li>
                                @if (!Auth::check())
                                    <a href="{{ route('shop.login') }}"><i class="fa fa-lock"></i>Login</a>
                                @else
                                    <a href="{{ route('shop.logout') }}"><i class="fa fa-lock"></i>Logout</a>
                                @endif
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--/header-middle-->

    <div class="header-bottom">
        <!--header-bottom-->
        <div class="container">
            <div class="row">
                <div class="col-sm-9">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse"
                            data-target=".navbar-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <div class="mainmenu pull-left">
                        <ul class="nav navbar-nav collapse navbar-collapse">
                            <li><a href="{{ route('index') }}" class="active">Home</a></li>
                            <li class="dropdown"><a href="{{ route('shop.blog') }}">Blog</a>
                            </li>
                            <li><a href="contact-us.html">Contact</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="search_box pull-right">
                        <form action="{{ route('shop.searchname') }}" method="GET">
                            @csrf
                            <input type="text" name="searchName" placeholder="Search" />
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--/header-bottom-->
</header>
<!--/header-->
