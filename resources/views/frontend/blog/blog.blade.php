@extends('frontend.layouts.master')
@section('title', 'Blog')
@section('content')
    <div class="col-sm-9">
        <div class="blog-post-area">
            <h2 class="title text-center">Blog</h2>
            @foreach ($data as $item)
                <div class="single-blog-post">
                    <h3 style="font-weight: bold;">{{ $item->title }}</h3>
                    <div class="post-meta">
                        {{-- <ul>
                            <li><i class="fa fa-user"></i> Mac Doe</li>
                            <li><i class="fa fa-clock-o"></i> 1:33 pm</li>
                            <li><i class="fa fa-calendar"></i> DEC 5, 2013</li>
                        </ul> --}}
                        {{-- <span>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star-half-o"></i>
                        </span> --}}
                    </div>
                    <a href="{{ route('shop.blog-single', ['id' => $item->id]) }}">
                        <img src="../upload/blog/image/{{ $item->image }}" alt="">
                    </a>
                    <p>{{ $item->description }}</p>
                    <a class="btn btn-primary" href="{{ route('shop.blog-single', ['id' => $item->id]) }}">Read More</a>
                </div>
            @endforeach
            <div class="pagination-area">
                {{ $data->links('vendor.pagination.bootstrap-4') }}
            </div>
        </div>
    </div>
@endsection
