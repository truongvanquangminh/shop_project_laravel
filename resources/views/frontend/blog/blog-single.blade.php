@extends('frontend.layouts.master')
@section('title', 'Blog Single')
@section('content')
    <div class="col-sm-9">
        <div class="blog-post-area">
            <h2 class="title text-center">Latest From our Blog</h2>
            <div class="single-blog-post">
                <h3>{{ $data->title }}</h3>
                <div class="post-meta">
                    {{-- <ul>
                        <li><i class="fa fa-user"></i> Mac Doe</li>
                        <li><i class="fa fa-clock-o"></i> 1:33 pm</li>
                        <li><i class="fa fa-calendar"></i> DEC 5, 2013</li>
                    </ul> --}}
                    <span>
                        <div class="rate">
                            <div class="vote">
                                @for ($i = 1; $i <= 5; $i++)
                                    <div
                                        class="star_{{ $i }} ratings_stars {{ $i <= $rate ? 'ratings_over' : '' }}">
                                        <input value="{{ $i }}" type="hidden">
                                    </div>
                                @endfor
                                <span class="rate-np">{{ $rate }}</span>
                            </div>
                        </div>
                    </span>
                </div>
                <a href="">
                    <img src="{{ URL::asset('/upload/blog/image/' . $data->image) }}" alt="">
                </a>
                {!! $data->content !!}
                <div class="pager-area">
                    <ul class="pagination pull-right">
                        <li>
                            @if ($previous)
                                <a href="{{ URL::to('shop/blog/' . $previous) }}">Previous</a>
                            @endif
                        </li>
                        <li>
                            @if ($next)
                                <a href="{{ URL::to('shop/blog/' . $next) }}">Next</a>
                            @endif
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!--/blog-post-area-->

        <div class="rating-area">
            <ul class="ratings">
                <li class="rate-this">Rate this item:</li>
                <li>
                    <div class="rate">
                        <div class="vote">
                            @for ($i = 1; $i < 6; $i++)
                                <div
                                    class="star_{{ $i }} ratings_stars {{ $i <= $rate ? 'ratings_over' : '' }}">
                                    <input value="{{ $i }}" type="hidden">
                                </div>
                            @endfor
                            <span class="rate-np">{{ $rate }}</span>
                        </div>
                    </div>
                </li>
            </ul>
            {{-- <ul class="tag">
                <li>TAG:</li>
                <li><a class="color" href="">Pink <span>/</span></a></li>
                <li><a class="color" href="">T-Shirt <span>/</span></a></li>
                <li><a class="color" href="">Girls</a></li>
            </ul> --}}
        </div>
        <!--/rating-area-->

        <!--Comments-->
        <div class="response-area">
            <h2>Comment</h2>
            <ul class="media-list">
                @foreach ($comments as $comment)
                    @if ($comment->level == 0)
                        <li class="media" id="{{ $comment->id }}">
                            <a class="pull-left" href="#">
                                <img class="media-object" src="{{ asset('upload/user/avatar/' . $comment->avatar) }}"
                                    style="width: 121px;height: 86px;" alt="">
                            </a>
                            <div class="media-body">
                                <ul class="sinlge-post-meta">
                                    <li><i class="fa fa-user"></i>{{ $comment->name }}</li>
                                    <li><i class="fa fa-clock-o"></i> {{ date_format($comment->created_at, 'g:i a') }}
                                    </li>
                                    <li><i class="fa fa-calendar"></i> {{ date_format($comment->created_at, 'F j, Y') }}
                                    </li>
                                </ul>
                                <p>{{ $comment->cmt }}</p>
                                <a class="btn btn-primary" href="#form"><i class="fa fa-reply"></i>Replay</a>
                            </div>
                        </li>
                    @endif
                    @foreach ($comments as $reply)
                        @if ($reply->level == $comment->id)
                            <li class="media second-media">
                                <a class="pull-left" href="#">
                                    <img class="media-object" src="{{ asset('upload/user/avatar/' . $reply->avatar) }}"
                                        style="width: 121px;height: 86px;" alt="">
                                </a>
                                <div class="media-body">
                                    <ul class="sinlge-post-meta">
                                        <li><i class="fa fa-user"></i>{{ $reply->name }}</li>
                                        <li><i class="fa fa-clock-o"></i> {{ date_format($reply->created_at, 'g:i a') }}
                                        </li>
                                        <li><i class="fa fa-calendar"></i>
                                            {{ date_format($reply->created_at, 'F j, Y') }}</li>
                                    </ul>
                                    <p>{{ $reply->cmt }}</p>
                                    <a class="btn btn-primary" href=""><i class="fa fa-reply"></i>Replay</a>
                                </div>
                            </li>
                        @endif
                    @endforeach
                @endforeach

            </ul>
        </div>
        <!--/Response-area-->
        <div class="replay-box">
            <div class="row">
                <div class="col-sm-4">
                    <h2>Leave a replay</h2>
                    {{-- <form>
                        <div class="blank-arrow">
                            <label>Your Name</label>
                        </div>
                        <span>*</span>
                        <input type="text" placeholder="write your name...">
                        <div class="blank-arrow">
                            <label>Email Address</label>
                        </div>
                        <span>*</span>
                        <input type="email" placeholder="your email address...">
                        <div class="blank-arrow">
                            <label>Web Site</label>
                        </div>
                        <input type="email" placeholder="current city...">
                    </form> --}}
                </div>
                <div class="col-sm-8">
                    <form action="{{ route('shop.comment', ['id' => $data->id]) }}" method="post" id="form">
                        @csrf
                        <div class="text-area">
                            <div class="blank-arrow">
                                <label>Comment</label>
                            </div>
                            <span>*</span>
                            <textarea name="comment" rows="11"></textarea>
                            <input type="hidden" name="idComment" class="idComment" value="0">
                            <button class="btn btn-primary" id="submit_cmt">Post comment</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!--/Repaly Box-->
    </div>
    <script>
        $(document).ready(function() {

            var checkLogin = '{{ auth()->check() }}';

            $('#submit_cmt').click(function() {
                if (!checkLogin) {
                    alert('Vui lòng đăng nhập để bình luận!');
                    return false;
                } else {
                    $(this).sumit();
                }
            })

            $('.btn-primary').click(function() {
                if (!checkLogin) {
                    alert('Vui lòng đăng nhập để trả lời comment');
                    return false;
                } else {
                    var idCommentParent = $(this).closest('li.media').attr('id');
                    $(this).attr('id', idCommentParent);
                    var getId = $(this).attr('id');
                    var idCommentChild = $('input.idComment').attr('value', getId);
                    $(this).load(attr('href'));
                }
            })

            //vote
            $('.ratings_stars').hover(
                // Handles the mouseover    
                function() {
                    $(this).prevAll().andSelf().addClass('ratings_hover');
                },
                function() {
                    $(this).prevAll().andSelf().removeClass('ratings_hover');
                }
            );

            $('.ratings_stars').click(function() {
                if (!checkLogin) {
                    alert('Vui lòng đăng nhập');
                } else {
                    var values = $(this).find("input").val();
                    var idBlog = '{{ $data->id }}';

                    if ($(this).hasClass('ratings_over')) {
                        $('.ratings_stars').removeClass('ratings_over');
                        $(this).prevAll().andSelf().addClass('ratings_over');
                    } else {
                        $(this).prevAll().andSelf().addClass('ratings_over');
                    }
                    $.ajax({
                        type: "POST",
                        url: "{{ route('shop.rate') }}",
                        data: {
                            _token: "{{ csrf_token() }}",
                            point: values,
                            idBlog: idBlog
                        },
                        success: function(data) {
                            console.log(data);
                        }
                    })
                    $('.rate-np').html(values);
                }
            });
        });
    </script>
@endsection
