<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Blog;
use App\Models\Comment;
use App\Models\Rate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BlogController extends Controller
{
    public function index()
    {
        $data = Blog::orderBy('id', 'desc')->paginate(3);
        return view('frontend.blog.blog', compact('data'));
    }

    public function blog_single($id)
    {
        $data = Blog::find($id);
        $previous = Blog::where('id', '<', $data->id)->max('id');
        $next = Blog::where('id', '>', $data->id)->min('id');

        $rating = Rate::where('blog_id', $id)->avg('point');
        $rate = round($rating);

        $comments = Comment::where('idBlog', $id)->get();
        
        return view('frontend.blog.blog-single', compact('data', 'previous', 'next', 'rate', 'comments'));
    }

    public function rate()
    {
        $blog_id = $_POST['idBlog'];
        $point = $_POST['point'];

        $rate = new Rate();
        $rate->user_id = Auth::user()->id;
        $rate->blog_id = $blog_id;
        $rate->point = $point;
        $rate->save();
    }
}
