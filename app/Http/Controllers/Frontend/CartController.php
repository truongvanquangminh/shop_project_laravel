<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Product;
use Illuminate\Http\Request;

class CartController extends Controller
{
    public function index()
    {
        if (session()->has('cart')) {
            $carts = session()->get('cart');
            return view('frontend.cart.index', compact('carts'));
        }
        return view('frontend.cart.index');
    }

    public function addCart(Request $request)
    {
        $id = $_POST['idProduct'];
        $product = Product::find($id);
        $getCart = session()->get('cart');
        if (isset($getCart[$id])) {
            $getCart[$id]['qty'] += 1;
        } else {
            $getCart[$id] = [
                'name' => $product['name'],
                'price' => $product['price'],
                'img' => current(json_decode($product['images'])),
                'user_id' => $product['user_id'],
                'created_at' => $product['created_at'],
                'qty' => 1
            ];
        }
        session()->put('cart', $getCart);

        $countProduct = count(session()->get('cart'));

        return response()->json([
            'success' => 'Add product to your cart successfully',
            'countProduct' => $countProduct
        ]);
    }

    public function cartQuantityUp()
    {
        $idProduct = $_POST['idProduct'];
        if (session()->has('cart')) {
            $cart = session()->get('cart');
            $total = 0;
            foreach ($cart as $key => $value) {
                if ($key == $idProduct) {
                    $cart[$key]['qty'] += 1;
                }
            }
            session()->put('cart', $cart);
            foreach ($cart as $key => $value) {
                $total += $value['price'] * $value['qty'];
            }
            return response()->json([
                'total' => $total
            ]);
        }
    }

    public function cartQuantityDown()
    {
        $idProduct = $_POST['idProduct'];
        if (session()->has('cart')) {
            $cart = session()->get('cart');
            $total = 0;
            $sum = 0;
            foreach ($cart as $key => $value) {
                if ($key == $idProduct) {
                    $cart[$key]['qty'] -= 1;
                }
                if ($cart[$key]['qty'] < 1) {
                    unset($cart[$key]);
                }
            }
            session()->put('cart', $cart);
            foreach ($cart as $key => $value) {
                $total -= $value['price'] * $value['qty'];
                $sum = -$total;
            }
            return response()->json([
                'total' => $total,
                'sum' => $sum
            ]);
        }
    }

    public function delete()
    {
        $idProduct = $_GET['idProduct'];
        if (session()->has('cart')) {
            $cart = session()->get('cart');
            $total = 0;
            foreach ($cart as $key => $value) {
                if ($key == $idProduct) {
                    unset($cart[$key]);
                }
            }
            session()->put('cart', $cart);
            foreach ($cart as $key => $value) {
                $total = $value['price'] * $value['qty'];
            }
            return response()->json([
                'total' => $total
            ]);
        }
    }
}
