<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Http\Requests\MemberLoginRequest;
use App\Http\Requests\MemberRegisterRequest;
use App\Http\Requests\UpdateAccountRequest;
use App\Models\Country;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MemberController extends Controller
{
    public function index()
    {
        return view('frontend.member.login');
    }

    public function login(MemberLoginRequest $request)
    {
        $login = [
            'email' => $request->email,
            'password' => $request->password,
            'level' => 0
        ];

        $remember = false;

        if ($request->remeber_me) {
            $remember = true;
        }

        if (Auth::attempt($login, $remember)) {
            return redirect(route('index'));
        }
        return redirect()->back()->withErrors('Email or password is not correct.');
    }

    public function register()
    {
        return view('frontend.member.register');
    }

    public function doRegister(MemberRegisterRequest $request)
    {
        $data = new User();
        $data->name = $request->name;
        $data->email = $request->email;
        $data->password = bcrypt($request->password);
        $data->phone = $request->phone;
        $data->address = $request->address;
        $file = $request->avatar;
        if (!empty($file)) {
            $data->avatar = $file->getClientOriginalName();
        }
        $data->level = 0;

        if ($data->save()) {
            if (!empty($file)) {
                $file->move('upload/user/avatar', $file->getClientOriginalName());
            }
            return redirect()->back()->with('success',_('Register success.'));
        }
    }

    public function logout()
    {
        Auth::logout();
        return redirect(route('shop.login'));
    }

    public function show()
    {
        $data = Country::all();
        return view('frontend.member.account', compact('data'));
    }

    public function updateAccount(UpdateAccountRequest $request)
    {
        $userID = Auth::id();
        $user = User::find($userID);
        $data = $request->all();
        $file = $request->avatar;

        if(!empty($file)){
            $data['avatar'] = $file->getClientOriginalName();
        }
        if (!$data['password']) {
            $data['password'] = bcrypt($data['password']);
        } else {
            $data['password'] = $user->password;
        }

        if($user->update($data)){
            if(!empty($file))
            {
                $file->move('upload/user/avatar', $file->getClientOriginalName());
            }
            return redirect()->back()->with('success',_('Cập nhật thông tin thành công'));
        }else{
            return redirect()->back()->withError('Cập nhật thông tin thất bại');
        }
    }
}
