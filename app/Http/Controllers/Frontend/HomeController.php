<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Brand;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        // $products = Product::limit(6)->latest()->get();
        $products = Product::latest()->paginate(6);
        return view('frontend.home', compact('products'));
    }

    public function showProduct($id)
    {
        $product = Product::find($id);
        return view('frontend.product.product-detail', compact('product'));
    }

    public function productCategory($id)
    {
        $getAllProduct = Product::where('category_id', $id)->get()->toArray();
        return view('frontend.product.category', compact('getAllProduct'));
    }

    public function productBrand($id)
    {
        $getAllProduct = Product::where('brand_id', $id)->get()->toArray();
        return view('frontend.product.brand', compact('getAllProduct'));
    }
}
