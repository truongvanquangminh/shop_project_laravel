<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\History;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class CheckoutController extends Controller
{
    public function index()
    {
        return view('frontend.checkout.index');
    }

    public function store(Request $request)
    {
        $total = 0;
        if (session()->has('cart')) {
            $carts = session()->get('cart');
            foreach ($carts as $key => $value) {
                $total += $value['price'] * $value['qty'];
            }
        }
        if (Auth::check()) {
            $user = Auth::user();

            //send email for user
            $emailBody = '';
            $emailTo = $user->email;
            $subject = "Mail order product";

            $emailBody .= '<p class="pTag">Chào ' . $user->name . ',</p><br><p class="pTag"> Đơn hàng của bạn đã đặt thành công! </p>';
            $emailcontent = array(
                'emailBody' => $emailBody
            );

            Mail::send(['html' => 'frontend.sendMail.mail'], $emailcontent, function ($message) use ($emailTo, $subject) {
                $message->from('kingluca2100@gmail.com', 'King Luca');
                $message->to($emailTo);
                $message->subject($subject);
            });

            // Save history
            History::create([
                'email' => $emailTo,
                'phone' => $user->phone,
                'name'  => $user->name,
                'id_user' => $user->id,
                'price' => $total
            ]);
        } else {
            $data = new User();
            $data->name = $request->name;
            $data->email = $request->email;
            $data->password = bcrypt($request->password);
            $data->phone = $request->phone;
            $data->address = $request->address;
            $file = $request->avatar;
            if (!empty($file)) {
                $data->avatar = $file->getClientOriginalName();
            }
            $data->level = 0;

            if ($data->save()) {
                if (!empty($file)) {
                    $file->move('upload/user/avatar', $file->getClientOriginalName());
                }
            }
            //send email for user
            $emailBody = '';
            $emailTo = $data->email;
            $subject = "Mail order product";

            $emailBody .= '<p class="pTag">Chào ' . $data->name . ',</p><br><p class="pTag"> Đơn hàng của bạn đã đặt thành công! </p>';
            $emailcontent = array(
                'emailBody' => $emailBody
            );

            Mail::send(['html' => 'frontend.sendMail.mail'], $emailcontent, function ($message) use ($emailTo, $subject) {
                $message->from('kingluca2100@gmail.com', 'King Luca');
                $message->to($emailTo);
                $message->subject($subject);
            });

            // Save history
            History::create([
                'email' => $emailTo,
                'phone' => $data->phone,
                'name'  => $data->name,
                'id_user' => $data->id,
                'price' => $total
            ]);
        }

        return redirect()->back()->with('success', _('Đặt hàng thành công. Vui lòng kiểm tra email'));
    }
}
