<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateProduct;
use App\Models\Brand;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image as Image;

class ProductController extends Controller
{
    public function index()
    {
        $products = Product::where('user_id', Auth::user()->id)->get();
        return view('frontend.product.my-product', compact('products'));
    }

    public function create()
    {
        $category = Category::all();
        $brand = Brand::all();
        return view('frontend.product.add-product', compact('category', 'brand'));
    }

    public function add(CreateProduct $request)
    {
        $date = date('Y/m/d');
        if ($request->hasfile('image')) {
            if (!is_dir('upload/product/' . Auth::user()->id . '/' . $date)) {
                mkdir('upload/product/' . Auth::user()->id . '/' . $date, 0777, true);
            }
            foreach ($request->file('image') as $image) {

                $name = $image->getClientOriginalName();
                $name_2 = "hinh85_" . $image->getClientOriginalName();
                $name_3 = "hinh329_" . $image->getClientOriginalName();

                $path = public_path('upload/product/' . Auth::user()->id . '/' . $date . '/' . $name);
                $path2 = public_path('upload/product/' . Auth::user()->id . '/' . $date . '/' . $name_2);
                $path3 = public_path('upload/product/' . Auth::user()->id . '/' . $date . '/' . $name_3);

                Image::make($image->getRealPath())->save($path);
                Image::make($image->getRealPath())->resize(85, 84)->save($path2);
                Image::make($image->getRealPath())->resize(329, 380)->save($path3);

                $data[] = $name;
            }
        }

        $product = new Product();
        $product->name = $request->name;
        $product->price = $request->price;
        $product->category_id = $request->category;
        $product->brand_id = $request->brand;
        $product->status = $request->status;
        if ($request->sale) {
            $product->sale = $request->sale;
        }
        $product->company = $request->company;
        $product->detail = $request->detail;
        $product->images = json_encode($data);
        $product->user_id = Auth::user()->id;
        if ($product->save())
            return back()->with('success', 'Thêm sản phẩm thành công');
    }

    public function show($id)
    {
        $product = Product::find($id);
        $categories = Category::all();
        $brands = Brand::all();
        return view('frontend.product.edit', compact('product', 'id', 'categories', 'brands'));
    }

    public function edit(Request $request, $id)
    {
        $product = Product::find($id);
        $product->name = $request->name;
        $product->price = $request->price;
        $product->category_id = $request->category;
        $product->brand_id = $request->brand;
        $product->status = $request->status;
        if ($request->sale != 0) {
            $product->sale = $request->sale;
        }
        $product->company = $request->company;
        $product->detail = $request->detail;

        $getArrImage = json_decode($product['images'], true);
        $file = $request->images;
        if (!empty($file)) {
            foreach ($getArrImage as $key => $image) {
                if (in_array($image, $file)) {
                    unset($getArrImage[$key]);
                }
            }
        }

        foreach ($getArrImage as $image) {
            $data[] = $image;
        }

        $date = date('Y/m/d');
        if ($request->hasfile('image')) {
            if (!is_dir('upload/product/' . Auth::user()->id . '/' . $date)) {
                mkdir('upload/product/' . Auth::user()->id . '/' . $date, 0777, true);
            }
            foreach ($request->file('image') as $image) {

                $name = $image->getClientOriginalName();
                $name_2 = "hinh85_" . $image->getClientOriginalName();
                $name_3 = "hinh329_" . $image->getClientOriginalName();

                $path = public_path('upload/product/' . Auth::user()->id . '/' . $date . '/' . $name);
                $path2 = public_path('upload/product/' . Auth::user()->id . '/' . $date . '/' . $name_2);
                $path3 = public_path('upload/product/' . Auth::user()->id . '/' . $date . '/' . $name_3);

                Image::make($image->getRealPath())->save($path);
                Image::make($image->getRealPath())->resize(85, 84)->save($path2);
                Image::make($image->getRealPath())->resize(329, 380)->save($path3);

                $data[] = $name;
            }
        }
        $countImg = count($data);
        if ($countImg <= 3) {
            $product->images = json_encode($data);
            if ($product->update())
                return back()->with('success', 'Cập nhật sản phẩm thành công');
        } return back()->withErrors('Vui lòng chỉ được tải 3 ảnh');
    }

    public function delete($id)
    {
        $product = Product::find($id);
        if ($product->delete()) {
            return redirect()->back()->with('success', _('Bạn đã xóa sản phẩm thành công'));
        }
        return redirect()->withErrors('Sản phẩm chưa được xóa');
    }
}
