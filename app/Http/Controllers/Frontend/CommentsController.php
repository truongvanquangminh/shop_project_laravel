<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Http\Requests\CommentsRequest;
use App\Models\Blog;
use App\Models\Comment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CommentsController extends Controller
{
    public function comment(CommentsRequest $request, $id)
    {
        $comment = new Comment();
        $comment->idBlog = $id;
        $comment->idUser = Auth::user()->id;
        $comment->name = Auth::user()->name;
        $comment->avatar = Auth::user()->avatar;
        $comment->cmt = $request->comment;
        $comment->level = $request->idComment;
        $comment->save();
        return redirect(route('shop.blog-single', ['id' => $id]));
    }
}
