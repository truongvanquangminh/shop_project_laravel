<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Brand;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function searchName(Request $request)
    {
        $products = Product::query();
        $categories = Category::all()->toArray();
        $brands = Brand::all()->toArray();
        $nameProduct = $request->searchName;
        if (!empty($nameProduct)) {
            $products = $products->where('name', 'like', "%{$nameProduct}%");
        }

        $products = $products->paginate(6);
        return view('frontend.search.index', compact('products', 'categories', 'brands'));
    }

    public function searchAdvanced()
    {
        $categories = Category::all()->toArray();
        $brands = Brand::all()->toArray();
        $nameProduct =  $_GET['nameProduct'];
        $price =  $_GET['price'];
        $category = $_GET['category'];
        $brand = $_GET['brand'];
        $status = $_GET['status'];
        $products = Product::query();

        if (!empty($nameProduct)) {
            $products->where('name', 'like', "%{$nameProduct}%");
        }

        if (!empty($price)) {
            $price = explode('-', $price);
            $products->whereBetween('price', [$price[0], $price[1]]);
        }

        if (!empty($category)) {
            $products->where('category_id', $category);
        }

        if (!empty($brand)) {
            $products->where('brand_id', $brand);
        }

        if (!empty($status)) {
            $products->where('status', $status);
        }

        $products = $products->paginate(9);
        return view('frontend.search.index', compact('products', 'categories', 'brands'));
    }

    public function priceRange()
    {
        $minPrice = $_GET['minPrice'];
        $maxPrice = $_GET['maxPrice'];

        // $products = Product::all()->whereBetween('price', [$minPrice, $maxPrice])->toArray();
        // return response()->json(['data'=>$products]);

        $products = Product::all()->whereBetween('price', [$minPrice, $maxPrice]);
        return view('frontend.search.price-range', compact('products'));
    }
}
