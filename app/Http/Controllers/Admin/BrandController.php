<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateBrand;
use App\Models\Brand;
use Illuminate\Http\Request;

class BrandController extends Controller
{
    public function index()
    {
        $data = Brand::paginate(5);
        return view('admin.brand.index', compact('data'));
    }

    public function create()
    {
        return view('admin.brand.add');
    }

    public function add(CreateBrand $request)
    {
        $data = new Brand();
        $data->name = $request->name;
        if ($data->save()) {
            return redirect(route('ad.brand'))->with('success', _('Thêm thành công'));
        }
        return redirect()->back()->withErrors('Thêm bị lỗi');
    }

    public function delete($id)
    {
        $data = Brand::where('id', $id)->delete();
        if ($data) {
            return redirect(route('ad.brand'))->with('success', _('Xóa thành công'));
        }
    }
}
