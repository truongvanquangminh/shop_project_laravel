<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateCountry;
use App\Models\Country;
use Illuminate\Http\Request;

class CountryController extends Controller
{
    public function index()
    {
        $data = Country::paginate(5);
        return view('admin.country.index', compact('data'));
    }

    public function create()
    {
        return view('admin.country.add');
    }

    public function add(CreateCountry $request)
    {
        $data = new Country();
        $data->name = $request->name;
        if ($data->save()) {
            return redirect(route('ad.country'))->with('success', _('Thêm thành công'));
        }
        return redirect()->back()->withErrors('Thêm bị lỗi');
    }

    public function show($id)
    {
        $data = Country::where('id', $id)->get();
        return view('admin.country.edit', compact('data', 'id'));
    }

    public function update($id)
    {
        $data = Country::where('id', $id)->update(['name' => $_POST['name']]);
        if ($data) {
            return redirect(route('ad.country'))->with('success', _('Sửa thành công'));
        }
    }

    public function delete($id)
    {
        $data = Country::where('id', $id)->delete();
        if ($data) {
            return redirect(route('ad.country'))->with('success', _('Xóa sản phẩm thành công'));
        }
    }
}
