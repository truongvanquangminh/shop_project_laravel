<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateCategory;
use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function index()
    {
        $data = Category::paginate(5);
        return view('admin.category.index', compact('data'));
    }

    public function create()
    {
        return view('admin.category.add');
    }

    public function add(CreateCategory $request)
    {
        $data = new Category();
        $data->name = $request->name;
        if ($data->save()) {
            return redirect(route('ad.category'))->with('success', _('Thêm thành công'));
        }
        return redirect()->back()->withErrors('Thêm bị lỗi');
    }

    public function delete($id)
    {
        $data = Category::where('id', $id)->delete();
        if ($data) {
            return redirect(route('ad.category'))->with('success', _('Xóa thành công'));
        }
    }
}
