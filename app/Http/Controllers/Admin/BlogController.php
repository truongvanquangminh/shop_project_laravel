<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\BlogRequest;
use App\Models\Blog;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    public function index()
    {
        $data = Blog::paginate(5);
        return view('admin.blog.index', compact('data'));
    }

    public function create()
    {
        return view('admin.blog.add');
    }

    public function add(BlogRequest $request)
    {
        $data = new Blog();
        $data->title = $request->tilte;
        $file = $request->image;
        if (!empty($file)) {
            $data->image = $file->getClientOriginalName();
        }
        $data->description = $request->desc;
        $data->content = $request->content;

        if ($data->save()) {
            if (!empty($file)) {
                $file->move('upload/blog/image', $file->getClientOriginalName());
            }
            return redirect(route('ad.blog'))->with('success', _('Thêm bài viết thành công'));
        }
        return redirect()->back()->withErrors('Thêm bài viết thất bại');
    }

    public function show($id)
    {
        $data = Blog::where('id', $id)->get();
        return view('admin.blog.edit', compact('data', 'id'));
    }

    public function edit(BlogRequest $request, $id)
    {
        $blog = Blog::find($id);
        $data = $request->all();
        $file = $request->image;
        if (!empty($file)) {
            $data['image'] = $file->getClientOriginalName();
        }

        if ($blog->update($data)) {
            if (!empty($file)) {
                $file->move('upload/blog/image', $file->getClientOriginalName());
            }
            return redirect(route('ad.blog'))->with('success', _('Sửa thành công'));
        }
    }

    public function delete($id)
    {
        $data = Blog::where('id', $id)->delete();
        if ($data) {
            return redirect(route('ad.blog'))->with('success', _('Xóa sản phẩm thành công'));
        }
    }
}
