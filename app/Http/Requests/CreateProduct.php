<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateProduct extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'price' => 'required | integer | not_in:0',
            'category' => 'required',
            'brand' => 'required',
            'company' => 'required',
            'image' => 'required',
            'image.*' => 'image|mimes:jpg,png,jpeg,gif,svg',
            'detail' => 'required'
        ];
    }
}
