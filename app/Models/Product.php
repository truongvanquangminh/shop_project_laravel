<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $table = 'products';

    public $timestamps = true;

    // protected $fillable = [
    //     'name',
    //     'price',
    //     'category_id',
    //     'brand_id',
    //     'status',
    //     'sale',
    //     'company',
    //     'images',
    //     'detail',
    //     'user_id',
    // ];
}
