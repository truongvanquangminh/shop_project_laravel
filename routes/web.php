<?php

use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Admin\CountryController;
use App\Http\Controllers\Admin\BlogController;
use App\Http\Controllers\Admin\BrandController;
use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Frontend\CommentsController;
use App\Http\Controllers\Frontend\BlogController as FrontendBlogController;
use App\Http\Controllers\Frontend\CartController;
use App\Http\Controllers\Frontend\CheckoutController;
use App\Http\Controllers\Frontend\HomeController;
use App\Http\Controllers\Frontend\MemberController;
use App\Http\Controllers\Frontend\ProductController;
use App\Http\Controllers\Frontend\SearchController;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['namespace' => 'Frontend'], function () {

    Route::get('/', [HomeController::class, 'index'])->name('index');
    Route::get('/product-detail/{id}', [HomeController::class, 'showProduct'])->name('shop.product-detail');
    Route::get('/product/category/{id}', [HomeController::class, 'productCategory']);
    Route::get('/product/brand/{id}', [HomeController::class, 'productBrand']);

    Route::group(['middleware' => 'usernotlogin'], function () {
        Route::get('/shopLogin', [MemberController::class, 'index'])->name('shop.login');
        Route::post('/shopLogin', [MemberController::class, 'login'])->name('shop.login');
        Route::get('/shopRegister', [MemberController::class, 'register'])->name('shop.register');
        Route::post('/shopRegister', [MemberController::class, 'doRegister'])->name('shop.register');
    });

    Route::get('/blog', [FrontendBlogController::class, 'index'])->name('shop.blog');
    Route::get('/blog/{id}', [FrontendBlogController::class, 'blog_single'])->name('shop.blog-single');
    Route::post('/blog/rate', [FrontendBlogController::class, 'rate'])->name('shop.rate');

    Route::post('/blog/{id}/comment', [CommentsController::class, 'comment'])->name('shop.comment');

    Route::middleware('user')->group(function () {
        Route::get('/logout', [MemberController::class, 'logout'])->name('shop.logout');
        Route::get('/account', [MemberController::class, 'show'])->name('shop.account');
        Route::post('/account', [MemberController::class, 'updateAccount'])->name('shop.update.account');

        Route::get('/product', [ProductController::class, 'index'])->name('shop.product');
        Route::get('/product/add', [ProductController::class, 'create'])->name('shop.product.create');
        Route::post('/product/add', [ProductController::class, 'add'])->name('shop.product.add');
        Route::get('/product/{id}', [ProductController::class, 'show'])->name('shop.product.show');
        Route::post('/product/edit/{id}', [ProductController::class, 'edit'])->name('shop.product.edit');
        Route::get('/product/delete/{id}', [ProductController::class, 'delete'])->name('shop.product.delete');

        Route::get('/showCart', [CartController::class, 'index'])->name('shop.showCart');
        Route::post('/addCart', [CartController::class, 'addCart'])->name('shop.addCart');
        Route::post('/cart-quantity-up', [CartController::class, 'cartQuantityUp'])->name('shop.cartQtyUp');
        Route::post('/cart-quantity-down', [CartController::class, 'cartQuantityDown'])->name('shop.cartQtyDown');
        Route::get('/deleteCart', [CartController::class, 'delete'])->name('shop.deleteCart');

        Route::get('/checkout', [CheckoutController::class, 'index'])->name('shop.checkout');
        Route::get('/sendEmail', [CheckoutController::class, 'store'])->name('shop.sendmail');
        Route::post('/sendEmail1', [CheckoutController::class, 'store'])->name('shop.sendmail1');

        Route::get('/search', [SearchController::class, 'searchName'])->name('shop.searchname');
        Route::get('/searchAvanced', [SearchController::class, 'searchAdvanced'])->name('shop.searchAvanced');
        Route::get('/price-range', [SearchController::class, 'priceRange'])->name('shop.priceRange');
    });
});

Auth::routes();

Route::group([
    'prefix' => 'admin',
    'namespace' => 'Auth'
], function () {
    Route::get('/login', [LoginController::class, 'showLoginForm']);
    Route::post('/login', [LoginController::class, 'login']);
    Route::get('/logout', [LoginController::class, 'logout']);
});

Route::group([
    'prefix' => 'admin',
    'namespace' => 'Admin',
    'middleware' => 'admin'
], function () {
    Route::get('/dashboard', [DashboardController::class, 'index'])->name('ad');

    Route::get('/profile', [UserController::class, 'index'])->name('ad.profile');
    Route::post('/profile/update', [UserController::class, 'update'])->name('ad.profile.update');

    Route::get('/country', [CountryController::class, 'index'])->name('ad.country');
    Route::get('/country/create', [CountryController::class, 'create'])->name('ad.country.create');
    Route::post('/country/add', [CountryController::class, 'add'])->name('ad.country.add');
    Route::get('/country/{id}', [CountryController::class, 'show'])->name('ad.country.show');
    Route::post('/country/edit/{id}', [CountryController::class, 'update'])->name('ad.country.edit');
    Route::get('/country/delete/{id}', [CountryController::class, 'delete'])->name('ad.country.delete');

    Route::get('/blog', [BlogController::class, 'index'])->name('ad.blog');
    Route::get('/blog/create', [BlogController::class, 'create'])->name('ad.blog.create');
    Route::post('/blog/add', [BlogController::class, 'add'])->name('ad.blog.add');
    Route::get('/blog/{id}', [BlogController::class, 'show'])->name('ad.blog.show');
    Route::post('/blog/edit/{id}', [BlogController::class, 'edit'])->name('ad.blog.edit');
    Route::get('/blog/delete/{id}', [BlogController::class, 'delete'])->name('ad.blog.delete');

    Route::get('/category', [CategoryController::class, 'index'])->name('ad.category');
    Route::get('/category/create', [CategoryController::class, 'create'])->name('ad.category.create');
    Route::post('/category/add', [CategoryController::class, 'add'])->name('ad.category.add');
    Route::get('/category/delete/{id}', [CategoryController::class, 'delete'])->name('ad.category.delete');

    Route::get('/brand', [BrandController::class, 'index'])->name('ad.brand');
    Route::get('/brand/create', [BrandController::class, 'create'])->name('ad.brand.create');
    Route::post('/brand/add', [BrandController::class, 'add'])->name('ad.brand.add');
    Route::get('/brand/delete/{id}', [BrandController::class, 'delete'])->name('ad.brand.delete');
});

Route::get('/clear-cache', function () {
    Artisan::call('cache:clear');
    return "Cache is cleared";
});
